#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>


/*
 *
 * Author: Luis Roma Barge 
 * Date: March 2020
 * 
 * This is 2D Heat Equation-like problem using stencil strategy.
 * Here a topology of processes is created using MPI functions.
 * Our problem has dimensions n*n but, having the halo into account, we reserve
 * (n+2) * (n+2) elements. This gives us two extra columns and rows in the boundaries
 * of the domain. Since we are doing 5 points stencil, each elements needs itself and
 * his 4 neighbours to calculate the new values. Each process will need to obtain the 
 * frontier values (outside of his region) from his neighbours.
 * 
 * 
 * 
 * Usage:
 *  Compile with make stencil or mpicc -std=c99 nei_stencil.c -o ptp_stencil.out
 *  Run with mpirun -n 1 ./nei_stencil.out 200 1 400
 * where n is the number of desired processes. 
 * 
 * 
 * 
 * Parameters:
 * 
 * argv[1] := Size of grid (nxn)
 * argv[2] := Energy to be introduce in the system
 * argv[3] := Number of iterations
 * 
 * 
 * 
 * Note: Arrays in here are created in a 1-D array way.
 * if you have a matrix with size n*n in a 2-D way you access
 * to his indices like this:
 * A[i][j]
 * Since our arrays are 1-D you acces like this:
 * A[i*n+j]
 * with n being the size of one side of the array.
 * 
 */

int main(int argc, char **argv) {

  //program variables
  int n, heat, iterations;


  //MPI variables
  
  // rank, number of processes
  int r,p;
  // Number of processes in each
  // direction in the topology
  int px,py;
  MPI_Comm comm = MPI_COMM_WORLD;

  //MPI is iniciated
  MPI_Init(&argc, &argv);
  // Rank of the process
  MPI_Comm_rank(comm, &r);
  // Number of processes
  MPI_Comm_size(comm, &p); 

// Only process 0 gets the data and broadcast it to others processes
  if (r==0) {
      if(argc < 4) {
          printf("Variables: <n> <heat> <iterations>\n");
          MPI_Finalize();
          exit(1);
      }

      // Size of the region (n*n)
      n = atoi(argv[1]);
      // Heat to be injected per iteration in the system
      heat = atoi(argv[2]); 
      // Number of iterations
      iterations = atoi(argv[3]); 

      // Initial variables distributed by broadcast
      int args[3] = {n, heat, iterations};
      MPI_Bcast(args, 3, MPI_INT, 0, comm);
  }
  else {
      // The rest of the processes get the data
      int args[3];
      MPI_Bcast(args, 3, MPI_INT, 0, comm);
      n=args[0]; heat=args[1]; iterations=args[2];
  }

  //Topology dimensions
  //initializated to 0 but mpi fills the numbers
  int pdims[2]={0,0};
  MPI_Dims_create(p, 2, pdims);
  px = pdims[0];
  py = pdims[1];

  // Topology
  //Set to 0 since  is not cyclic
  int periods[2] = {0,0};
  MPI_Comm tology;
  MPI_Cart_create(comm, 2, pdims, periods, 0, &tology);

  // Here each process gets to know its neighbours
  int up, down, right, left;
  // x, positive shift
  MPI_Cart_shift(tology, 1, 1, &left, &right);
  // y, positive shift
  MPI_Cart_shift(tology, 0, 1, &up, &down);
  



  // rx, ry positions of each process in the topology
  int coords[2];
  MPI_Cart_coords(tology, r, 2, coords);
  int rx = coords[0];
  int ry = coords[1];

  /*
   * Example:
   * Imagine we have 4 processes. p = 4 and
   * r = 0,1,2,3 depending of process
   * 
   * we choose px = 2 and py = 2
   * 
   * For process with rank r=0
   * rx = 0, ry = 0
   * For process with rank r=1
   * rx = 1, ry = 0
   * For process with rank r=2
   * rx = 0, ry = 1
   * For process with rank r=3
   * rx = 1, ry = 1
   * 
   */

  // The computations are divided
  int nx=n/px;
  int ny=n/py;

  // You need to calculate the offset
  // Starting point of each process in the array
  int offnx=rx*nx;
  int offny=ry*ny;

  // We need the arrays to have (n+2) * (n+2)
  // size because of the one-sized halo zone
  double *aold = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *anew = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *tmp;

  // This is the number of heat sources in the system
  // We chose it to be three.
  #define nsources 3
  // The location of these sources
  int sources[nsources][2] = {{n/2,n/2}, {n/3,n/3}, {3*n/4,3*n/4}};


  // The sources must be locally referenced
  // Number of sources in my zone
  int nls=0;
  // Local sources array
  int locals[nsources][2];
  // Local sources calculations using offset
  for (size_t i =0 ; i < nsources; i++) {
    int locx=sources[i][0]-offnx;
    int locy=sources[i][1]-offny;
  // Checking if it is inside of the zone
    if(locx >= 0 && locx < nx && locy >= 0 && locy < ny) {
      locals[nls][0] = locx+1; // offset ny halo zone
      locals[nls][1] = locy+1; // offset ny halo zone
      nls++;
    }
  }

  double t=-MPI_Wtime(); // take time

  //size is nx+nx+ny+ny
  double *sbuf = (double*)calloc(2*nx*sizeof(double)+2*ny,sizeof(double)); // send buffer (west, east, north, south)
  double *rbuf = (double*)calloc(2*nx*sizeof(double)+2*ny,sizeof(double)); // receive buffer (w, e, n, s)

  double total_heat;
  for(int iter=0; iter<iterations; ++iter) {




    // Exchange data with neighbors


    // Data into send buffer

    // Left (1,y)
    for(int i=0; i<ny; ++i)
      sbuf[i] = aold[1*(n+2)+i+1];
    // Right (nx,y)
    for(int i=0; i<ny; ++i)
      sbuf[ny+i] = aold[nx*(n+2)+i+1];
    // Up (x,1)
    for(int i=0; i<nx; ++i)
      sbuf[2*ny+i] = aold[(i+1)*(n+2)+1];
    // Down (x,ny)
    for(int i=0; i<nx; ++i)
      sbuf[2*ny+nx+i] = aold[(i+1)*(n+2)+ny];

    // Counts and Displacements
    int counts[4] = {ny, ny, nx, nx};
    int displs[4] = {0, ny, 2*ny, 2*ny+nx};
    // Request nonblocking
    MPI_Request req;
    // Status info
    MPI_Status status;
    //here all the communications are done
    MPI_Ineighbor_alltoallv(sbuf, counts, displs, MPI_DOUBLE, rbuf, counts, displs, MPI_DOUBLE, tology, &req);

    //Here we could do concurrent calculations.

    //wait for the requests to complete
    MPI_Wait(&req, &status);

    //recv data into aold
    for(int i=0; i<ny; ++i)
      aold[(0*(n+2)+i+1)] = rbuf[i];
    for(int i=0; i<ny; ++i)
      aold[(nx+1)*(n+2)+i+1] = rbuf[ny+i];
    for(int i=0; i<nx; ++i)
      aold[(i+1)*(n+2)+0] = rbuf[2*ny+i];
    for(int i=0; i<nx; ++i)
      aold[(i+1)*(n+2)+ny+1] = rbuf[2*ny+nx+i];


      // Equation is applied to get anew
    total_heat = 0.0;
    for(size_t i=1; i<nx+1; ++i) {
      for(size_t j=1; j<ny+1; ++j) {
        anew[i*(n+2)+j] = aold[i*(n+2)+j]/2.0 
          + (aold[(i-1)*(n+2)+j]
          + aold[(i+1)*(n+2)+j]
          + aold[i*(n+2)+j-1]
          + aold[i*(n+2)+j+1])/8.0;


    // In these zones we introduce heat into the system
        total_heat += anew[i*(n+2)+j];
      }
    }


    for(size_t i=0; i<nls; ++i) {
      anew[locals[i][0]*(n+2)+locals[i][1]] += heat;
    }

    //update Aold becomes Anew. Anew will get recalculated next iteration.
    tmp=anew; anew=aold; aold=tmp;

  }
  t+=MPI_Wtime();

  // Final heal summation of partial heat
  double rheat;
  MPI_Allreduce(&total_heat, &rheat, 1, MPI_DOUBLE, MPI_SUM, comm);
  if(r == 0){
    printf("total heat: %f time: %f\n", rheat, t);
  }
  MPI_Finalize();
  return 0;
}
