#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>



/*
 *
 * Author: Luis Roma Barge 
 * Date: March 2020
 * 
 * This is 2D Heat Equation-like problem using stencil strategy.
 * In this program Isend and Irecv functions are used. These are non-blocking
 * functions.
 * In this program a topology of processes is created manually.
 * Our problem has dimensions n*n but, having the halo into account, we reserve
 * (n+2) * (n+2) elements. This gives us two extra columns and rows in the boundaries
 * of the domain. Since we are doing 5 points stencil, each elements needs itself and
 * his 4 neighbours to calculate the new values. Each process will need to obtain the 
 * frontier values (outside of his region) from his neighbours.
 * 
 * 
 * 
 * Usage:
 *  Compile with make stencil or mpicc -std=c99 ptp_stencil.c -o ptp_stencil.out
 *  Run with mpirun -n 1 ./ptp_stencil.out 200 1 400 2 2
 * where n is the number of desired processes. 
 * 
 * 
 * Parameters:
 * 
 * argv[1] := Size of grid (nxn)
 * argv[2] := Energy to be introduce in the system
 * argv[3] := Number of iterations
 * argv[4] := Number of processes in x direction
 * argv[5] := Number of processes in y direction
 * 
 * 
 * 
 * Note: Arrays in here are created in a 1-D array way.
 * if you have a matrix with size n*n in a 2-D way you access
 * to his indices like this:
 * A[i][j]
 * Since our arrays are 1-D you acces like this:
 * A[i*n+j]
 * with n being the size of one side of the array.
 * 
 */


int main(int argc, char **argv) {

  //program variables
  int n, heat, iterations;


  //MPI variables
  
  // rank, number of processes
  int r,p;
  // Number of processes in each
  // direction in the topology
  int px,py;
  MPI_Comm comm = MPI_COMM_WORLD;
  // In here we store respective neighbour location
  int up, down, left,right;


  //MPI is iniciated
  MPI_Init(&argc, &argv);
  // Rank of the process
  MPI_Comm_rank(comm, &r);
  // Number of processes
  MPI_Comm_size(comm, &p);


// Only process 0 gets the data and broadcast it to others processes
  if (r==0) {
      if(argc < 6) {
          printf("Variables: <n> <heat> <niters> <px> <py>\n");
          MPI_Finalize();
          exit(1);
      }


      // Size of the region (n*n)
      n = atoi(argv[1]);
      // Heat to be injected per iteration in the system
      heat = atoi(argv[2]); 
      // Number of iterations
      iterations = atoi(argv[3]); 
      // topology size on x
      px=atoi(argv[4]); 
      // topology size on y
      py=atoi(argv[5]); 

      // px * py must be p
      if(px * py != p) MPI_Abort(comm, 101);
      // n must be a factor of px and py
      if(n % py != 0) MPI_Abort(comm, 102); 
      if(n % px != 0) MPI_Abort(comm, 103);

      // Initial variables distributed by broadcast
      int args[5] = {n, heat, iterations, px,  py};
      MPI_Bcast(args, 5, MPI_INT, 0, comm);
  }
  else {
      // The rest of the processes get the data
      int args[5];
      MPI_Bcast(args, 5, MPI_INT, 0, comm);
      n=args[0]; heat=args[1]; iterations=args[2]; px=args[3]; py=args[4];
  }

  //positions of each rank in the topology
  int rx,ry;

  rx=r%px;
  ry=r/px;

  /*
   * Example:
   * Imagine we have 4 processes. p = 4 and
   * r = 0,1,2,3 depending of process
   * 
   * we choose px = 2 and py = 2
   * 
   * For process with rank r=0
   * rx = 0, ry = 0
   * For process with rank r=1
   * rx = 1, ry = 0
   * For process with rank r=2
   * rx = 0, ry = 1
   * For process with rank r=3
   * rx = 1, ry = 1
   * 
   */

  // Obtaining the neighbours
  up = (ry-1)*px+rx;
  down = (ry+1)*px+rx;
  left= ry*px+rx-1;
  right = ry*px+rx+1;


  // Topology is not cyclic. 
  // Boundary processes have no neighbours
  if(ry-1 < 0)   up = MPI_PROC_NULL;
  if(ry+1 >= py) down = MPI_PROC_NULL;
  if(rx-1 < 0)   left = MPI_PROC_NULL;
  if(rx+1 >= px) right = MPI_PROC_NULL;

  // The computations are divided
  int nx=n/px;
  int ny=n/py;

  // You need to calculate the offset
  // Starting point of each process in the array
  int offnx=rx*nx;
  int offny=ry*ny;

  // We need the arrays to have (n+2) * (n+2)
  // size because of the one-sized halo zone
  double *aold = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *anew = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *tmp;



  // This is the number of heat sources in the system
  // We chose it to be three.
  #define nsources 3
  // The location of these sources
  int sources[nsources][2] = {{n/2,n/2}, {n/3,n/3}, {3*n/4,3*n/4}};


  // The sources must be locally referenced
  // Number of sources in my zone
  int nls=0;
  // Local sources array
  int locals[nsources][2];
  // Local sources calculations using offset
  for (size_t i =0 ; i < nsources; i++) {
    int locx=sources[i][0]-offnx;
    int locy=sources[i][1]-offny;
  // Checking if it is inside of the zone
    if(locx >= 0 && locx < nx && locy >= 0 && locy < ny) {
      locals[nls][0] = locx+1; // offset by halo zone
      locals[nls][1] = locy+1; // offset by halo zone
      nls++;
    }
  }

  double t=-MPI_Wtime(); // take time


  // You need to have buffers where the sending information is copied.

  // Send buffers
  double *sbufup = (double*)calloc(nx,sizeof(double));
  double *sbufdown = (double*)calloc(nx,sizeof(double));
  double *sbufright = (double*)calloc(ny,sizeof(double));
  double *sbufleft = (double*)calloc(ny,sizeof(double));
  // Recv buffers
  double *rbufup = (double*)calloc(nx,sizeof(double));
  double *rbufdown = (double*)calloc(nx,sizeof(double));
  double *rbufright = (double*)calloc(ny,sizeof(double));
  double *rbufleft = (double*)calloc(ny,sizeof(double));

  double total_heat;
  for(int iter=0; iter<iterations; ++iter) {



    //Each process do 8 communitations (4 send 4 recv) so we need 8 requests
    MPI_Request reqs[8];

    //Data copied into the buffers

    // Up
    for(size_t i=0; i<nx; ++i)
      sbufup[i] = aold[(i+1)*(n+2)+1];
    // Down
    for(size_t i=0; i<nx; ++i)
      sbufdown[i] = aold[(i+1)*(n+2)+ny];
    // Right
    for(size_t i=0; i<ny; ++i)
      sbufright[i] = aold[nx*(n+2)+(i+1)];
    // Left
    for(size_t i=0; i<ny; ++i)
      sbufleft[i] = aold[1*(n+2)+(i+1)];

    // Send & Recv
    MPI_Isend(sbufup, nx, MPI_DOUBLE, up, 123, comm, &reqs[0]);
    MPI_Isend(sbufdown, nx, MPI_DOUBLE, down, 123, comm, &reqs[1]);
    MPI_Isend(sbufright, ny, MPI_DOUBLE, right, 123, comm, &reqs[2]);
    MPI_Isend(sbufleft, ny, MPI_DOUBLE, left, 123, comm, &reqs[3]);
    MPI_Irecv(rbufup, nx, MPI_DOUBLE, up, 123, comm, &reqs[4]);
    MPI_Irecv(rbufdown, nx, MPI_DOUBLE, down, 123, comm, &reqs[5]);
    MPI_Irecv(rbufright, ny, MPI_DOUBLE, right, 123, comm, &reqs[6]);
    MPI_Irecv(rbufleft, ny, MPI_DOUBLE, left, 123, comm, &reqs[7]);

    // Here we could do concurrent calculations because 
    // We are using Isend, Irecv (non-blocking) communication


    // Wait for the requests to complete
    MPI_Waitall(8, reqs, MPI_STATUSES_IGNORE);

    // Recv data into aold
    for(size_t i=0; i<nx; ++i)
      aold[(i+1)*(n+2)+0] = rbufup[i];
    for(size_t i=0; i<nx; ++i)
      aold[(i+1)*(n+2)+ny+1] = rbufdown[i];
    for(size_t i=0; i<ny; ++i)
      aold[(nx+1)*(n+2)+i+1] = rbufright[i];
    for(size_t i=0; i<ny; ++i)
      aold[0*(n+2)+i+1] = rbufleft[i];

    // Equation is applied to get anew
    total_heat = 0.0;
    for(size_t i=1; i<nx+1; ++i) {
      for(size_t j=1; j<ny+1; ++j) {
        anew[i*(n+2)+j] = aold[i*(n+2)+j]/2.0 
          + (aold[(i-1)*(n+2)+j]
          + aold[(i+1)*(n+2)+j]
          + aold[i*(n+2)+j-1]
          + aold[i*(n+2)+j+1])/8.0;

        // We add the total heat just for parallel comparations
        total_heat += anew[i*(n+2)+j];
      }
    }

    // In these zones we introduce heat into the system
    for(size_t i=0; i<nls; ++i) {
      anew[locals[i][0]*(n+2)+locals[i][1]] += heat; // heat source
    }


    //update Aold becomes Anew. Anew will get recalculated next iteration.
    tmp=anew; anew=aold; aold=tmp;


  }
  t+=MPI_Wtime();

  // Final heal summation of partial heat
  double rheat;
  MPI_Allreduce(&total_heat, &rheat, 1, MPI_DOUBLE, MPI_SUM, comm);
  if(r == 0){
    printf("total heat: %f time: %f\n", rheat, t);
  }
  MPI_Finalize();
  return 0;
}
