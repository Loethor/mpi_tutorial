#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>


/*
 *
 * Author: Luis Roma Barge 
 * Date: March 2020
 * 
 * This is 2D Heat Equation-like problem using stencil strategy.
 *
 * Usage:
 *  Compile with make stencil or mpicc -std=c99 stencil.c -o stencil.out
 *  Run with mpirun -n 1 stencil.out 200 1 400
 * where n is the number of desired processes. 
 * 
 * Parameters:
 * 
 * argv[1] := Size of grid (nxn)
 * argv[2] := Energy to be introduce in the system
 * argv[3] := Number of iterations
 * 
 * 
 * 
 * 
 * Note: Arrays in here are created in a 1-D array way.
 * if you have a matrix with size n*n in a 2-D way you access
 * to his indices like this:
 * A[i][j]
 * Since our arrays are 1-D you acces like this:
 * A[i*n+j]
 * with n being the size of one side of the array.
 * 
 */




int main(int argc, char **argv) {

    if(argc < 4) {
      printf("Variables: <n> <heat> <iterations>\n");
      exit(1);
  }

  int n = atoi(argv[1]); // nxn grid
  int heat = atoi(argv[2]); // energy to be injected per iteration
  int iterations = atoi(argv[3]); // number of iterations

  // We need the arrays to have (n+2) * (n+2)
  // size because of the one-sized halo zone
  double *aold = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *anew = (double*) calloc((n+2)*(n+2),sizeof(double));
  double *tmp;


  // Create the MPI context
  MPI_Init(NULL, NULL);
  MPI_Comm comm = MPI_COMM_WORLD;
  // rank, number of processes
  int r,p;
  // Rank of the process
  MPI_Comm_rank(comm, &r);
  // Number of processes
  MPI_Comm_size(comm, &p);

  // This is the number of heat sources in the system
  // We chose it to be three.
  #define nsources 3
  // The location of these sources
  int sources[nsources][2] = {{n/2,n/2}, {n/3,n/3}, {n*4/5,n*8/9}};

  // Total heat in system
  double total_heat=0.0;

  // Time measure
  double t=-MPI_Wtime();

  // The range of the loops goes from 1 to n (instead of regular 0 to n-1).
  // This is because 0 and n+1 are the halo zones.
  for(int iter=0; iter<iterations; ++iter) {
    total_heat=0.0;
    for(int i=1; i<n+1; ++i) {
      for(int j=1; j<n+1; ++j) {
        // Here we apply the equation
        anew[i*(n+2)+j] = aold[i*(n+2)+j]/2.0 
          + (aold[(i-1)*(n+2)+j]
          + aold[(i+1)*(n+2)+j]
          + aold[i*(n+2)+j-1]
          + aold[i*(n+2)+j+1])/8.0;

        // We just add the total heat for parallel comparations
        total_heat += anew[i*(n+2)+j];
      }
    }

    // In these zones we introduce heat into the system
    for(int i=0; i<nsources; ++i) {
      anew[(sources[i][0])*(n+2)+sources[i][1]] += heat;
    }

    //update Aold becomes Anew. Anew will get recalculated next iteration.
    tmp=anew; anew=aold; aold=tmp;
  }
  t+=MPI_Wtime();

  // Print total heat of last iteration.
  if(r == 0){
    printf("total heat: %f time: %f\n", total_heat, t);
  }
  MPI_Finalize();
  return 0;
}
